console.log("Hello WOrld")

let posts = [];
let count = 1;

// Add post data 
	// addEventListener(event, callback function that will be triggered if the event occur of happen)
	// type can be used for event"submit"
	document.querySelector("#form-add-post").addEventListener("submit", (event) =>{
		event.preventDefault();

		posts.push({
			id: count,
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value
		})
		/*console.log(posts);*/

		count++;
		showPosts(posts)
	})

// Show posts 
	// function expression is not hoisted
	const showPosts = (posts) =>{
		let postEntries = ``;

		posts.forEach((post) => {
			postEntries += `<div id= "post-${post.id}">
			<h3 id= "post-title-${post.id}">${post.title}</h3>
			<p id= "post-body-${post.id}">${post.body}</p>
			<button onclick = "editPost('${post.id}')">Edit</button>
			<button onclick = "deletePost('${post.id}')">Delete</button>
			</div>`
		})

		/*console.log(postEntries)*/

		document.querySelector("#div-post-entries").innerHTML = postEntries
	}

// Edit post 

const editPost = (id)=>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

/*	console.log(`This is the title of the post: ${title}`);
	console.log(`This is the body of the post: ${body}`);*/

	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
	document.querySelector("#txt-edit-id").value = id
}

/*const deletePost = (id)=> {
	let title = document.querySelector(`#post-title-${id}`).innerHTML ="";
	let body = document.querySelector(`#post-body-${id}`).innerHTML ="";
	posts.pop({posts})

}*/

// deletepost
const deletePost = (id) => {
	posts.splice(id-1, 1);
	document.querySelector(`#post-${id}`).remove()
}

document.querySelector("#form-edit-post").addEventListener("submit", (event) => {
	event.preventDefault();

	const idToBeEdited = document.querySelector(`#txt-edit-id`).value;

	for(let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === idToBeEdited){
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value
			showPosts(posts);
			alert("Edit is successful")
			break;
		}
	}

})
